# BUS ROUTE CHALLENGE #

### Technology used ###

* Java 8
* Spring Boot
* Eclipse Collections
* Google Guava

### Solution description ###

* Reverse route to stations structure to station -> routes which O(N_R*M_R) where N_R number of routes, and M_R maximum station per route
* Now we have O(1) access to routes per station
* Intersect sets of routes per station to find first match O(N)

### Comments ###
I increased sleep time in script to 1 minutes from 5 seconds - the file with maximum upper limits per entity can take up to 50 seconds to load.