package busservice.component;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import org.eclipse.collections.impl.set.mutable.primitive.IntHashSet;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.ResourceUtils;


@RunWith(MockitoJUnitRunner.class)
public class DataImporterTest {

  private DataImporter routesImporter = new DataImporter();

  @Test
  public void normalFileTest() throws Exception {
    File file = ResourceUtils.getFile(this
        .getClass()
        .getResource("/dataFile"));

    Map<Integer, IntHashSet> stationToRoutes = routesImporter.parseFile(file.getPath());

    assertThat("Parsed structure should contains 7 elements as keys", stationToRoutes.size(),
        is(7));

    assertThat("Station 0 should appears with 0 2 3 routes",
        stationToRoutes.get(0),
        contains(0,2,3)
    );

    assertThat("Station 1 should appears with 0 1 routes",
        stationToRoutes.get(1),
        contains(0,1)
    );

    assertThat("Station 2 should appears with 0 route",
        stationToRoutes.get(2),
        contains(0)
    );

    assertThat("Station 3 should appears with 0 1 routes",
        stationToRoutes.get(3),
        contains(0, 1)
    );

    assertThat("Station 4 should appears with 0 2 3 routes",
        stationToRoutes.get(4),
        contains(0,2,3)
    );

    assertThat("Station 5 should appears with 1 route",
        stationToRoutes.get(5),
        contains(1)
    );

    assertThat("Station 6 should appears with 1,2,3 routes",
        stationToRoutes.get(6),
        contains(1,2,3)
    );
  }

  @Test(expected = RuntimeException.class)
  public void corruptedFileTest() throws Exception {
    File file = ResourceUtils.getFile(this
        .getClass()
        .getResource("/corruptedDataFile"));

    routesImporter.parseFile(file.getPath());
  }

  @Test(expected = RuntimeException.class)
  public void fileNotExistTest() throws IOException {
    routesImporter.parseFile(null);
  }

  @Test(expected = RuntimeException.class)
  public void emptyFileTest() throws Exception {
    File file = ResourceUtils.getFile(this
        .getClass()
        .getResource("/emptyDataFile"));

    routesImporter.parseFile(file.getPath());
  }

  private Matcher<IntHashSet> contains(final int... compareItems) {
    return new BaseMatcher<IntHashSet>() {
      @Override
      public boolean matches(final Object item) {
        return Arrays.equals(compareItems, ((IntHashSet) item).toArray());
      }

      @Override
      public void describeTo(final Description description) {
      }
    };
  }

}
