package busservice.controller;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.nio.charset.Charset;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import busservice.BusRouteApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRouteApplication.class, properties = {
    "filePath=./build/resources/test/dataFile",
})
@WebAppConfiguration
public class BusRouteConrollerTest {
  private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
      MediaType.APPLICATION_JSON.getSubtype(),
      Charset.forName("utf8"));

  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @Before
  public void setup() throws Exception {
    this.mockMvc = webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void directRouteFound() throws Exception {
    mockMvc.perform(get("/api/direct")
        .param("dep_sid", "1")
        .param("arr_sid", "4"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(contentType))
        .andExpect(jsonPath("dep_sid", is(1)))
        .andExpect(jsonPath("arr_sid", is(4)))
        .andExpect(jsonPath("direct_bus_route", is(true)));
  }

  @Test
  public void directRouteNotFound() throws Exception {
    mockMvc.perform(get("/api/direct")
        .param("dep_sid", "9")
        .param("arr_sid", "15"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(contentType))
        .andExpect(jsonPath("dep_sid", is(9)))
        .andExpect(jsonPath("arr_sid", is(15)))
        .andExpect(jsonPath("direct_bus_route", is(false)));
  }
}
