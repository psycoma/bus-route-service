package busservice.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.eclipse.collections.impl.set.mutable.primitive.IntHashSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import busservice.repository.InMemoryRepository;

@RunWith(MockitoJUnitRunner.class)
public class BusRouteServiceTest {

  private BusRouteService busRouteService;

  @Mock
  private InMemoryRepository storageMock;

  @Before
  public  void init(){
    busRouteService = new BusRouteService(storageMock);
  }

  @Test
  public void directRouteTest() {

    when(storageMock.getRoutesByStationId(0)).thenReturn(Optional.of(new IntHashSet(1, 2, 3)));
    when(storageMock.getRoutesByStationId(4)).thenReturn(Optional.of(new IntHashSet(3, 6)));

    assertThat("Both stations available for route 3", busRouteService.hasDirectRoute(0, 4), is(true));
  }

  @Test
  public void invalidStationTest() {

    when(storageMock.getRoutesByStationId(0)).thenReturn(Optional.of(new IntHashSet(1, 2, 3)));
    when(storageMock.getRoutesByStationId(4)).thenReturn(Optional.empty());

    assertThat("No routes for station 4 exist", busRouteService.hasDirectRoute(0, 4), is(false));
  }

}
