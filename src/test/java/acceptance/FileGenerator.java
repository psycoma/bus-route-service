package acceptance;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.collect.Lists;

public class FileGenerator {

  private static final int ROUTES_AMOUNT = 100000;
  private static final int STATION_AMOUNT = 1000;

  public static void main(String[] args) throws IOException {
    List<String> lines = Lists.newArrayList();
    lines.add(header());
    lines.addAll(content());

    Path path = Paths.get("./bigFile");
    Files.write(path, lines, StandardCharsets.UTF_8);

  }

  private static Collection<? extends String> content() {
    return IntStream
        .range(0, ROUTES_AMOUNT - 1)
        .boxed()
        .map(route -> {
          StringBuffer routeStations = new StringBuffer();
          routeStations.append(route);
          routeStations.append(" ");

          new Random()
              .ints(STATION_AMOUNT, 0, 1000000)
              .boxed()
              .collect(Collectors.toSet()).stream().forEach(uniqueStationId -> {
            routeStations.append(uniqueStationId);
            routeStations.append(" ");
          });

          return routeStations.toString();
        })
        .collect(Collectors.toList());
  }

  private static String header() {
    return String.valueOf(ROUTES_AMOUNT - 1);
  }
}
