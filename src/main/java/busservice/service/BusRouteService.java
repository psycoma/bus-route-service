package busservice.service;

import java.util.Optional;

import org.eclipse.collections.impl.set.mutable.primitive.IntHashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import busservice.repository.InMemoryRepository;

@Service
public class BusRouteService {

  private static final Logger logger = LoggerFactory.getLogger(BusRouteService.class);

  private InMemoryRepository inMemoryRepository;

  public BusRouteService(@Autowired InMemoryRepository inMemoryRepository) {
    this.inMemoryRepository = inMemoryRepository;
  }

  @Cacheable("directRoutes")
  public boolean hasDirectRoute(int departureStationId, int arrivalStationId) {
    Optional<IntHashSet> routesForDepartureStation =
        inMemoryRepository.getRoutesByStationId(departureStationId);

    if (!routesForDepartureStation.isPresent()) {
      logger.info(String
          .format("Routes for departure station with id = %d doesn`t exist", departureStationId));

      return false;
    }

    Optional<IntHashSet> routesForArrivalStation =
        inMemoryRepository.getRoutesByStationId(arrivalStationId);

    if (!routesForArrivalStation.isPresent()) {
      logger.info(
          String.format("Routes for arrival station with id = %d doesnt exist", arrivalStationId));

      return false;
    }

    return routesForDepartureStation.get().anySatisfy(
        value -> routesForArrivalStation.get().contains(value));
  }

}
