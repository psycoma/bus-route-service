package busservice.component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.eclipse.collections.impl.list.mutable.primitive.IntArrayList;
import org.eclipse.collections.impl.set.mutable.primitive.IntHashSet;
import org.springframework.stereotype.Component;

import com.google.common.base.Splitter;
import com.google.common.collect.Maps;

@Component
public class DataImporter {

  /**
   * Reading file from specified location and perform some basic verification
   *
   * @return map of bus station to routes
   */
  public Map<Integer, IntHashSet> parseFile(String filePath) throws IOException {

    Map<Integer, IntHashSet> stationToRoutes = Maps.newHashMap();

    IntArrayList recordsCount = new IntArrayList();

    Files
        .lines(Paths.get(filePath))
        .skip(1)
        .forEach(line -> {

          List<String> splittedString = Splitter.on(' ')
              .omitEmptyStrings()
              .trimResults()
              .splitToList(line);

          int[] stationIds = splittedString
              .stream()
              .skip(1)
              .mapToInt(Integer::parseInt)
              .toArray();

          int routeId = Integer.parseInt(splittedString.get(0));

          recordsCount.add(routeId);

          Arrays.stream(stationIds)
              .boxed()
              .forEach(stationId -> {
                stationToRoutes.computeIfAbsent(stationId, integer -> new IntHashSet());
                stationToRoutes.get(stationId).add(routeId);
              });
        });

    int amountOfRoutes = Files.lines(Paths.get(filePath))
        .findFirst()
        .map(Integer::parseInt)
        .get();

    if (recordsCount.size() != amountOfRoutes) {
      throw new RuntimeException("file is corrupted");
    }

    return stationToRoutes;

  }

}


