package busservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RouteResponse {

  @JsonProperty(value = "dep_sid", required = true)
  private Integer departureStationId;
  @JsonProperty(value = "arr_sid", required = true)
  private Integer arrivalStationId;
  @JsonProperty(value = "direct_bus_route", required = true)
  private Boolean directBusRoute;

  public RouteResponse() {
  }

  public RouteResponse(Integer departureStationId, Integer arrivalStationId,
      Boolean directBusRoute) {
    this.departureStationId = departureStationId;
    this.arrivalStationId = arrivalStationId;
    this.directBusRoute = directBusRoute;
  }

  public Integer getDepartureStationId() {
    return departureStationId;
  }

  public void setDepartureStationId(Integer departureStationId) {
    this.departureStationId = departureStationId;
  }

  public Integer getArrivalStationId() {
    return arrivalStationId;
  }

  public void setArrivalStationId(Integer arrivalStationId) {
    this.arrivalStationId = arrivalStationId;
  }

  public Boolean getDirectBusRoute() {
    return directBusRoute;
  }

  public void setDirectBusRoute(Boolean directBusRoute) {
    this.directBusRoute = directBusRoute;
  }
}
