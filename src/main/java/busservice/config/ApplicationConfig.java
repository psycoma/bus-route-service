package busservice.config;

import java.util.Map;

import org.eclipse.collections.impl.set.mutable.primitive.IntHashSet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import busservice.component.DataImporter;
import busservice.repository.InMemoryRepository;
import busservice.service.ShutdownService;

@Configuration
@PropertySource(value = "classpath:application.properties")
public class ApplicationConfig {

  @Bean
  public InMemoryRepository inMemoryRepository(@Value("${filePath}") String filePath,
      DataImporter dataImporter,
      ShutdownService shutdownService) {
    Map<Integer, IntHashSet> stationToRouteIds = null;

    try {
      stationToRouteIds = dataImporter.parseFile(filePath);
    } catch (Exception e) {
      e.printStackTrace();
      //if file was not parsed properly - no reason to continue, shutting down
      shutdownService.initiateShutdown();
    }

    return new InMemoryRepository(stationToRouteIds);
  }
}
