package busservice.repository;

import java.util.Map;
import java.util.Optional;

import org.eclipse.collections.impl.set.mutable.primitive.IntHashSet;
import org.springframework.stereotype.Repository;

@Repository
public class InMemoryRepository {

  private Map<Integer, IntHashSet> stationToRoutes;

  public InMemoryRepository(Map<Integer, IntHashSet> stationToRoutes) {
    this.stationToRoutes = stationToRoutes;
  }

  public Optional<IntHashSet> getRoutesByStationId(Integer stationId) {
    return Optional.ofNullable(stationToRoutes.get(stationId));
  }

}
