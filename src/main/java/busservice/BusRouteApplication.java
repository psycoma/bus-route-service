package busservice;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BusRouteApplication {

  public static void main(String[] args) {
    SpringApplication application = new SpringApplication(BusRouteApplication.class);

    application.setBannerMode(Banner.Mode.OFF);
    application.setLogStartupInfo(false);
    application.run(args);
  }
}
