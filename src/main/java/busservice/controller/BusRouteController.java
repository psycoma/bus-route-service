package busservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import busservice.model.RouteResponse;
import busservice.service.BusRouteService;

@Controller
@RequestMapping("/api/direct")
public class BusRouteController {

  @Autowired
  private BusRouteService busRouteService;

  @RequestMapping(method = RequestMethod.GET)
  public @ResponseBody RouteResponse isDirectBusRoute(
      @RequestParam(value = "dep_sid", required = true) Integer departureStationId,
      @RequestParam(value = "arr_sid", required = true) Integer arrivalStationId) {

    boolean isDirect = busRouteService.hasDirectRoute(departureStationId, arrivalStationId);

    return new RouteResponse(departureStationId, arrivalStationId, isDirect);
  }

}
